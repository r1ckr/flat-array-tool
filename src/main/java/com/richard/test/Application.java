package com.richard.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * flat-array-tool Main class!
 *
 */
public class Application
{
    public static void main( String[] args )
    {

        if (args.length < 1){
            //Show usage
            System.out.println("Usage");
            System.out.println("java -jar flat-array-tool.jar <nested-array>");
            System.exit(-1);
        }

        String nestedArray = args[0];
        String plainArray = nestedArray.replaceAll("[^0-9,]", "");

        List<Integer> intList = new LinkedList<Integer>();
        for (String numberStr : plainArray.split(",")){
            if(!numberStr.isEmpty())
            intList.add(new Integer(numberStr));
        }

        System.out.println(Arrays.toString(intList.toArray()).replace(" ",""));

    }
}

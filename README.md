# Flat Array Tool

Write some code, that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4]

Run with:
mvn package && java -jar target/flat-array-tool-1.0-SNAPSHOT.jar